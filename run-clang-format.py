#!/usr/bin/python3

import os
import sys
import subprocess
import yaml
from components import CommonUtils


configuration = yaml.safe_load(open(os.path.join(CommonUtils.scriptsBaseDirectory(), 'config', 'global.yml')))
if os.path.exists('.kde-ci.yml'):
    localConfig = yaml.safe_load(open('.kde-ci.yml'))
    CommonUtils.recursiveUpdate(configuration, localConfig)

versions = configuration['Options']['clang-format-versions']
for version in versions:
    if version < 14:
        print("Clang format versions lower than 14 are not installed on CI. Please enforce a newer version")
        sys.exit(1)
    format_cmd = f'find . -name "*.cpp" -or -name "*.h"  -or -name "*.c"  -or -name "*.cc" | xargs clang-format-{version} -i'
    print(f"Formatting with clang-format version {version}")
    res = subprocess.run(format_cmd, shell=True, stdout=sys.stdout, stderr=sys.stderr)
    if res.returncode != 0:
        sys.exit(1)
    res = subprocess.run(["git", "diff", "--exit-code"], stdout=sys.stdout, stderr=sys.stderr)
    if res.returncode != 0:
        sys.exit(1)
