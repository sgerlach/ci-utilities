# CI Utilities

This repository is the main collection for tools relating to KDE's Gitlab CI.

KDE uses `.kde-ci.yml` and `.gitlab-ci.yml` files. The first is used to specify
project dependencies, and the latter to run pipelines with simple include files.
For more details, see the wiki page
[https://community.kde.org/Infrastructure/Continuous_Integration_System].

## Structure

The root directory includes the main [config template](config-template.yml)
that you can use as a base for `.kde-ci.yml` files, as well as individual tools
used in the templates themselves.

* [components/](components): shared scripts used to generate templates
* [config/global.yml](config/global.yml): the default values for `.kde-ci.yml` files
* [craft/](craft): the configuration files for the [Craft](https://community.kde.org/Craft) templates
* [gitlab-templates/](gitlab-templates): the templates that can be included in `.gitlab-ci.yml` files in your project repository
* [resources/](resources): miscellaneous configuration files, schemas and scripts
* [signing/](signing): the configuration files used by the [KDE CI Notary Service](https://invent.kde.org/sysadmin/ci-notary-service) to publish websites and for signing: Android APKs, Windows APPX/MSIX, MacOS APP/DMGs, and Flatpaks (see the [list of available services](https://invent.kde.org/sysadmin/ci-notary-service#services))

## Tools

### run-cppcheck.py

Used by the [cppcheck CI template](gitlab-templates/cppcheck.yml).
To run the script locally for your project do the following.
* Set up everything for running the tool:
  * Clone the ci-utilities repository in your checkout of your project. Alternatively,
  add a link to a checkout of the ci-utilities repository.
  * Create a virtual Python environment for the tool:
```
python3 -m venv .venv
. .venv/bin/activate
pip install -r ci-utilities/resources/run-cppcheck-requirements.txt
```
* If you have already set up everything then just activate the virtual Python environment:
```
. .venv/bin/activate
```
* Run the tool:
```
python3 -u ci-utilities/run-cppcheck.py --project PROJECT_NAME
```
where you replace `PROJECT_NAME` with the (repository) name of your project.
